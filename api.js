const express = require('express');
const router = express.Router();

function genid() {
    let str = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let batata = "";
    for(let i=0; i<19; i++)
        batata += str[Math.floor(Math.random() * str.length)];

    return batata;
}

const servers = [
    {
          id: genid()
        , name: 'Structs'
        , users: []
    }
];

router.get('/', (req, res) => res.status(200).json('O Jorge é fixe'));

router.get('/servers', (req, res) => {
    res.status(200).json(servers);
});

router.get('/createServer', (req, res) => {
    const { user, server } = req.session;
    if(!user)
        res.status(401).json('401 Unauthorized');
    else {
        if(server)
            res.status(401).json('403 Forbidden');
        else {
            const { name } = req.query;
            if(!name)
                res.status(400).json('400 Bad Request');
            else {
                servers.push({
                    id: genid(),
                    name: name,
                    users: [{username: user.username, id: user.id}]
                });

                res.status(200).json(servers);
            }
        }
    }
});

module.exports = router;