const express = require('express');
const app = express();
app.disable('view cache');
const bodyParser = require('body-parser');
const session = require('cookie-session');
const cookieParser = require('cookie-parser');
const snekfetch = require('snekfetch');
const http = require('http').Server(app);
const port = 8080; // eslint-disable-line
var io = require('socket.io')(http);

function genid() {
    let str = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let batata = "";
    for(let i=0; i<25; i++)
        batata += str[Math.floor(Math.random() * str.length)];

    return batata;
}

app.use(cookieParser());
app.use(session({ secret: 'o stor jorge e fixe' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(`${__dirname}/src`));
app.set('views', `${__dirname}/src/views`);
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    const user = req.session.user;
    if(user)
        res.redirect('home');
    else
        res.render('index');
});

app.get('/home', async(req, res) => {
    const user = req.session.user;
    if(user) {
        const { body } = await snekfetch.get(`http://localhost:${port}/api/servers`);
        res.render('home', {user: user, servers: body});
    } else
        res.redirect('/');
});

app.get('/server/:serverid', async(req, res) => {
    const user = req.session.user;

    if(!user)
        res.redirect('/');
    else {
        const { serverid } = req.params;
        const servers = (await snekfetch.get(`http://localhost:${port}/api/servers`)).body;

        for(let idx=0; idx<servers.length; idx++) {
            if(servers[idx].id == serverid) {
                res.render('server', {server: servers[idx], user: user});
                break;
            }
        }

        res.redirect('/');
    }
});

app.get('/login', (req, res) => {
    const user = req.session.user;
    if(user)
        res.redirect('/logoff');
    else {
        req.session.user = {id: genid(), username: req.query.name};

        res.redirect('/home');
    }
});

app.get('/logout', (req, res) => {
    req.session.user = null;
    req.session.server = null;
    res.redirect('/');
});

app.use('/api', require('./api.js'));

app.use((req, res) => {
    res.status(404).send('404 Not Found');
});

io.on('connection', function(socket){
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });

    socket.on('server', function(status){
        io.emit('server', status);
    });
});

http.listen(port, () => console.log(`ok ${port}`));